import os
import sys
from skimage import io
from scipy import ndimage
import numpy as np
import matplotlib.pyplot as plt
import cv2

H1 = np.array([[0, 0, -1, 0, 0],
         [0, -1, -2, -1, 0],
         [-1, -2, 16, -2, -1],
         [0, -1, -2, -1, 0],
         [0, 0, -1, 0, 0]])

H2 =(1/256)* np.array([[1, 4, 6, 4, 1],
                    [4, 16, 24, 16, 4],
                    [6, 24, 36, 24, 6],
                    [4, 16, 24, 16, 4],
                    [1,  4, 6, 4, 1]])

H3 = np.array([[-1, 0, 1],
               [-2, 0, 2],
               [-1, 0, 1]])

H4 = np.array([[-1, -2, -1],
               [0, 0, 0],
               [1, 2, 1]])

H5 = np.array([[-1, -1, -1],
                [-1,  8, -1],
                [-1, -1, -1]])

H6 = (1/9)*np.array([[1, 1, 1],
                    [1, 1, 1],
                    [1, 1, 1]])

H7 = np.array([[-1, -1, 2],
               [-1, 2, -1],
               [2, -1, -1]])

H8 = np.array([[2, -1, -1],
              [-1, 2, -1],
              [-1, -1, 2]])

def convolve(img, kernel):
    k = np.asarray(kernel)
    print("Kernel ", k)

    out=cv2.filter2D(img, cv2.CV_8U, cv2.flip(kernel,0),
        anchor=(kernel.shape[1]-1, kernel.shape[0]-1), borderType=cv2.BORDER_REPLICATE)
    out = np.clip(out,0,255)
    return out.astype(np.uint8)

if __name__ == '__main__':
    assert len(sys.argv)==2, "You should use image path as first argument"
    path = sys.argv[1]
    img = io.imread(path)
    print("Shape: ", img.shape, " Type: ", img.dtype)

    os.makedirs('results/',exist_ok=True)
    img_h1 = convolve(img, H1)
    io.imsave('results/h1.png', img_h1)

    img_h2 = convolve(img, H2)
    io.imsave('results/h2.png', img_h2)

    img_h3 = convolve(img, H3)
    io.imsave('results/h3.png', img_h3)

    img_h4 = convolve(img, H4)
    io.imsave('results/h4.png', img_h4)

    img_h5 = convolve(img, H5)
    io.imsave('results/h5.png', img_h5)

    img_h6 = convolve(img, H6)
    io.imsave('results/h6.png', img_h6)

    img_h7 = convolve(img, H7)
    io.imsave('results/h7.png', img_h7)

    img_h8 = convolve(img, H8)
    io.imsave('results/h8.png', img_h8)

    img_h3_h4 = np.sqrt((img_h3/255)**2 + (img_h4/255)**2)
    img_h3_h4 = np.clip(img_h3_h4*255,0,255).astype(np.uint8)
    io.imsave('results/h3_h4.png', img_h3_h4)
